import { withPluginApi } from 'discourse/lib/plugin-api';

function initializePlugin(api)
{
  const siteSettings = api.container.lookup('site-settings:main');

  if (siteSettings.progressive_summarization_enabled) {
    // Add highlight button to the toolbar.
    api.onToolbarCreate(toolbar => {
      toolbar.addButton({
        id: "highlight_button",
        group: "fontStyles",
        icon: "paint-brush",
        shortcut: 'H',
        perform: e => e.applySurround('<mark>', '</mark>', 'highlight_default_text')
      });
    });
  }
}

export default
{
  name: 'progressive-summarization',
  initialize(container)
  {
    withPluginApi('0.1', api => initializePlugin(api));
  }
};