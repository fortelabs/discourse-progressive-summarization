import { registerOption } from 'pretty-text/pretty-text';

registerOption((siteSettings, opts) => {
  opts.features['progressive-summarization'] = siteSettings.progressive_summarization_enabled;
});

export function setup(helper) {
  helper.whiteList([ 'mark' ]);
}