# name: Progressive Summarization
# about: Adds Progressive Summarization capability (from Building a Second Brain, a Forte Labs Online course).
# version: 0.3.1
# authors: Benjamin Mosior
# url: https://gitlab.com/fortelabs/discourse-progressive-summarization

enabled_site_setting :progressive_summarization_enabled
