require 'rails_helper'

describe PrettyText do

  context 'markdown it' do
    before do
      SiteSetting.enable_experimental_markdown_it = true
    end

    it 'can properly bake highlights' do
      md = <<~MD
        This is a <mark>highlighted phrase</mark>.
      MD

      html = <<~HTML
        <p>This is a <mark>highlighted phrase</mark>.</p>
      HTML
      cooked = PrettyText.cook(md)
      expect(cooked).to eq(html.strip)
    end
  end
end